package main

import (
    "fmt"
	"os"
    "bufio"
	"io/ioutil"
	"text/template"
	"strings"
	"strconv"
)

type Point struct {
	X int
	Y int
}

func main() {
	tmplData, err := ioutil.ReadFile("svg.tmpl")
	if err != nil { panic(err) }

	tmpl, err := template.New("svg").Parse(string(tmplData))
	if err != nil { panic(err) }

	file, err := os.Open("pendigits.tes")
	if err != nil { panic(err) }
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	line := 0
	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())
		points := make([]Point, 8)
		j := 0
		for i := range fields {
			t, err := strconv.Atoi(fields[i])
			if err != nil { panic(err) }
			if i < 16 {
				if i % 2 == 0 {
					points[j].X = t
				} else {
					points[j].Y = 100 - t // SVGs are inverted
					j = j + 1
				}
			}
		}
		td := map[string]interface{}{
            "P0": points[0],
            "P1": points[1],
            "Points": points[2:8],
            "Char": fields[len(fields)-1],
        }
		svgFile := fmt.Sprintf("%05d-%s.svg", line, td["Char"])
		svg, err := os.Create(svgFile)
		if err != nil { panic(err) }
		tmpl.Execute(svg, td)
		svg.Close()
		line = line + 1
	}
}
